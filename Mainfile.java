

import java.io.*;
import java.lang.reflect.Method;
import java.net.*;
import java.text.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
public class Mainfile {

    public static void main(String[] args) throws IOException {
        // TODO Auto-generated method stub
        ServerSocket ss = new ServerSocket(3000);

        // running infinite loop for getting
        // client request
        while (true)
        {
            Socket s = null;

            try
            {
                // socket object to receive incoming client requests
                s = ss.accept();

                System.out.println("A new client is connected : " + s);

                DataInputStream dis = new DataInputStream(s.getInputStream());
                DataOutputStream dos = new DataOutputStream(s.getOutputStream());
                String msg = dis.readUTF();

                if(msg.equals("@#@jar"))
                {
                Thread t = new ClientHandler(s, dis, dos);
                // Invoking the start() method
                t.start();
            }
            else
            {
                Thread t1 = new Request(s,dis,dos,msg);
                t1.start();
            }
                }

                 catch (Exception e){
                s.close();
                e.printStackTrace();
            }
        }
    }

}
class ClientHandler extends Thread
{
    final DataInputStream dis;
    final DataOutputStream dos;
    final Socket s;


    // Constructor
    public ClientHandler(Socket s, DataInputStream dis, DataOutputStream dos)
    {
        this.s = s;
        this.dis = dis;
        this.dos = dos;
    }

    @Override
    public void run()
    {
        try {
        BufferedInputStream is = new BufferedInputStream(s.getInputStream());
        BufferedOutputStream os = new BufferedOutputStream(s.getOutputStream());
        
        DataInputStream dis = new DataInputStream(s.getInputStream());
        DataOutputStream dos = new DataOutputStream(s.getOutputStream());
        String fname = dis.readUTF();
        System.out.println("fname = "+ fname);
        File f=new File(fname);
        int filesize = (int) dis.readLong();
        FileOutputStream fos = new FileOutputStream(f);
		byte[] buffer = new byte[4096];
		int read = 0;
		int totalRead = 0;
		int remaining = filesize;
		while((read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
			totalRead += read;
			remaining -= read;
			System.out.println("read " + totalRead + " bytes.");
			fos.write(buffer, 0, read);
		}
		
		System.out.println();
		fos.close();
		is.close();
        os.close();
        s.close();
        }
        catch(Exception e) {

            System.out.println("Error:"+e);
        }
    }
}

class Request extends Thread{

	final DataInputStream dis;
    final DataOutputStream dos;
    final Socket s;
    final String func ;

    // Constructor
    public Request(Socket s, DataInputStream dis, DataOutputStream dos,String func)
    {
        this.s = s;
        this.dis = dis;
        this.dos = dos;
        this.func = func;
    }
    public void run()
    {
        try {
	        
        	
        	System.out.println(func);
	        String [] splitMsg = func.split("@#@");
	        String [] client= splitMsg[0].split(":");
	        String clientid=client[0];
	        String port=client[1];
	        
			String pathToJar = clientid + ".jar";
			String methodName = splitMsg[1];
			int noOfParams = Integer.parseInt(splitMsg[2]);
			
			JarFile jarFile = null;
			try {
				jarFile = new JarFile(pathToJar);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        Enumeration<JarEntry> e = jarFile.entries();
	        URL[] urls = { new URL("jar:file:" + pathToJar+"!/") };
	        URLClassLoader cl = URLClassLoader.newInstance(urls);

	        while (e.hasMoreElements()) {
	            JarEntry je = e.nextElement();
	            if(je.isDirectory() || !je.getName().endsWith(".class")) {
	                continue;
	            }
	            String className = je.getName().substring(0,je.getName().length()-6);
	            className = className.replace('/', '.');
	            Class<?> c = cl.loadClass(className);
	            Object o = c.newInstance();
	            
	            Class pval[] = null;
	            
	            Method meths[] = c.getDeclaredMethods();
	            for(Method m : meths) {
	            	String m1 = m.getName();
	            	if (m1.equals(methodName)) {
	            		pval = m.getParameterTypes();
	            	}
	            }
	            
	            //Object [] objs = new Object[noOfParams];
	            Object [] objs = new Object[noOfParams];
	            int j = 0;
	            for(int i = 0 ; i < noOfParams; i++) {
	            	switch (pval[i].toString()) {
					case "int":
						objs[i] = (Integer.parseInt(splitMsg[4 + j]));
						break;
					case "float":
						objs[i] = (Float.parseFloat(splitMsg[4 + j]));
						break;
					case "boolean":
						objs[i] = (Boolean.parseBoolean(splitMsg[4 + j]));
						break;
					case "long":
						objs[i] = (Long.parseLong(splitMsg[4 + j]));
						break;
					case "short":
						objs[i] = (Short.parseShort(splitMsg[4 + j]));
						break;
					case "double":
						objs[i] = (Double.parseDouble(splitMsg[4 + j]));
						break;

					default:
						objs[i] = (String)splitMsg[4 + j];
						break;
					}
	            	j+=2;
	            }
	            
	            Method method = o.getClass().getMethod(methodName, pval);
	            Class<?> returnType = method.getReturnType();
	            method.setAccessible(true);
	            
	            Object res = method.invoke(o, objs);
	            System.out.println(res);
	            String response = splitMsg[0] + "@#@" + res.toString();
	            dos.writeUTF(response);
	            
	            
	            dos.close();
	            break;
	        }

		}
	                
        catch(Exception e){
        	e.printStackTrace();
        }
      }
}