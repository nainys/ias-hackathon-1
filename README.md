# Group Number 2
# Link to the code : 
	https://bitbucket.org/nainys/ias-hackathon-1
# Master Part
	## Server Module (Master.java):
	Server module reads the slave agent's ip address and port to create a connection with them. After creating connection jar files are sent to all the slave agents.Whenever a request comes, server balances the load by picking the ipaddress of the slave agent with least number of requests. After the request is processed, the result is sent to the server and the server sends the result back to the client.
# Client Part
	## Client module:
	The client is sending the request to the master server. At the client side first the interface file is provided to the code generator and the generated file is used as an interface to satisfy the client's request. The client is sending the request in the form of string using marshalling.
# Slave Part
	## Slave Module:
	Slave Side Working:

	The slaves can receive two types of requests(implemented using multithreading)from the Master-

	1. Request for receiving a jar file provided by some client.
	2. Request for execution of some services, ordered by some client.

	These requests are segregated using some wired protocol.
	We have loaded the classes using classLoader and used reflections API to read the class information.
	This class information is used to serve the requests from the clients.
	Using the definitions provided by the clients in the jar files as provided by the clients we process the request and send the result back to the Master from where the result is sent to the client whose request was to be served.

