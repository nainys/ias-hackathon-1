import java.io.*;
import java.net.*;
import java.util.*;

public class ClntStub implements Operations
{

public int mul (int param1, int param2)
{
try {
	Integer p1 = new Integer(param1);
	Integer p2 = new Integer(param2);
	Socket client = new Socket("10.3.1.187", 20777);
	String s = "mul@#@2@#@int@#@"+p1+"@#@int@#@"+p2+"";
	PrintStream ps = new PrintStream(client.getOutputStream());
	ps.println(s);
	ps.flush();
	System.out.println(s);
	PrintStream ps1 = new PrintStream(client.getOutputStream());
	ps1.println("Master return the result!");
	ps1.flush();
	BufferedReader br= new BufferedReader(new InputStreamReader( client.getInputStream()));
	String str= br.readLine();
	System.out.println(str);
	ps.close();
	ps1.close();
	Integer res = Integer.parseInt(str);
	return res.intValue();
} 
catch (Exception e) {
	e.printStackTrace();
}
	return 0;
}

public int add (int param1, int param2)
{
try {
	Integer p1 = new Integer(param1);
	Integer p2 = new Integer(param2);
	Socket client = new Socket("10.3.1.187", 20777);
	String s = "add@#@2@#@int@#@"+p1+"@#@int@#@"+p2+"";
	PrintStream ps = new PrintStream(client.getOutputStream());
	ps.println(s);
	ps.flush();
	System.out.println(s);
	PrintStream ps1 = new PrintStream(client.getOutputStream());
	ps1.println("Master return the result!");
	ps1.flush();
	BufferedReader br= new BufferedReader(new InputStreamReader( client.getInputStream()));
	String str= br.readLine();
	System.out.println(str);
	ps.close();
	ps1.close();
	Integer res = Integer.parseInt(str);
	return res.intValue();
} 
catch (Exception e) {
	e.printStackTrace();
}
	return 0;
}
public static void main(String[] args)
{
	int i = 1;
	ClntStub cl = new ClntStub();
	for(;i<1001;i++)
	{
		cl.add(i,2);
		//cl.mul(i,6);
	}
}
}
