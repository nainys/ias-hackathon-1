import java.io.*;
import java.net.*;
import java.util.*;
public class Master extends Thread 
{
	
	private ServerSocket ss;
	static HashMap<String, Integer> hash = new HashMap<>();
	// Create load balancer map
	static HashMap<String, Integer> lb = new HashMap<>();
	public Master() throws IOException
	{
		ss = null;
		
	}
	public Master(int port)
	{

		try {
			
	
			ss = new ServerSocket();
			ss.setReuseAddress(true);
			ss.bind(new InetSocketAddress(port));

			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	//////////// LOAD BALANCER STARTS ////////////////
	private static void sortByValues()
	{ 
	       List list = new LinkedList(lb.entrySet());
	       // Defined Custom Comparator here
	       Collections.sort(list, new Comparator()
	       {
	            public int compare(Object o1, Object o2)
	            {
	               return ((Comparable) ((Map.Entry) (o1)).getValue())
	                  .compareTo(((Map.Entry) (o2)).getValue());
	            }
	       });

	       // Here I am copying the sorted list in HashMap
	       // using LinkedHashMap to preserve the insertion order
	       HashMap sortedHashMap = new LinkedHashMap();
	       for (Iterator it = list.iterator(); it.hasNext();)
	       {
	              Map.Entry entry = (Map.Entry) it.next();
	              sortedHashMap.put(entry.getKey(), entry.getValue());
	       } 
	       lb = sortedHashMap;
	  }
	synchronized static String getSlave()
	{
		Set set3 = lb.entrySet();//map1.entrySet();
	    Iterator iterator3 = set3.iterator();
	    	Map.Entry me3 = (Map.Entry)iterator3.next();
	    	lb.put((String)me3.getKey(), (Integer)me3.getValue()+1);

	   sortByValues();
	   return (String)me3.getKey();
	}
	
	synchronized static void requestServed(String ipaddress)
	{
		lb.put(ipaddress, lb.get(ipaddress)-1);
		sortByValues();
	}
	////// LOAD BALANCER ENDS!!   ///////////
	
	
	public void run()
	{
		while(true)
		{
			try {
				Socket cs = ss.accept();
				System.out.println("Connection accepted");
				
				
				Thread t = new ClientHandler(cs);
				 
                // Invoking the start() method
                t.start();
				
			}
			catch(Exception e)
			{
				System.out.println("catch of ss");
				e.printStackTrace();
			}
			
		}
	}
	
	
////////////////// Send method call ////////////////////////////////////////////
	public void sendMethodCall(Socket cs) throws IOException {
		BufferedReader br;
//		DataInputStream dis = new DataInputStream(cs.getInputStream());
		InputStream is = cs.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		br = new BufferedReader(isr);
		int port = cs.getPort();
		InetAddress inet = cs.getInetAddress();
		String ip = inet.getHostAddress();
		System.out.println("Client ip = "+ip);
		System.out.println("Client port = "+String.valueOf(port));
		
//		String msg = dis.readUTF();
		String msg = br.readLine();
		System.out.println("Message from client  == "+msg);
		

		String msg1 = ip+":"+port+"@#@"+msg;
		//GET IP FROM LOAD BALANCER!
		String slaveip = getSlave();
		int slaveport = hash.get(slaveip);
		Socket slavesend = new Socket(slaveip,slaveport);
		 DataOutputStream dos = new DataOutputStream(slavesend.getOutputStream());
		dos.writeUTF(msg1);
		
		InputStream iss = slavesend.getInputStream();
		InputStreamReader isrr = new InputStreamReader(iss);
		BufferedReader brr = new BufferedReader(isrr);
		
		PrintStream ps = new PrintStream(cs.getOutputStream());
		
		try {
//			Socket client = slaveacc.accept();
			String line = brr.readLine();
			System.out.println("output from slave  "+line);
			String[] clientdetails = line.split("@#@");
//			String[] client_ip_port = clientdetails[0].split(":");
//			String clientip = client_ip_port[0];
//			int clientport = Integer.parseInt(client_ip_port[1]);
			
			
			ps.println(clientdetails[1]);
			requestServed(slaveip);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		
	}


	public static void main(String args[]) throws IOException
	{
		
		// Read config file
		  File file = new File("Slaves");
		 
		  BufferedReader br = new BufferedReader(new FileReader(file));
		  
		  String st;
		  while ((st = br.readLine()) != null)
		  {
			  String ar[] = st.split(":");
			  hash.put(ar[0], Integer.parseInt(ar[1]));
			  lb.put(ar[0], 0);
		  }
		  
		  for (HashMap.Entry<String, Integer> entry : hash.entrySet()) {
			    String key = entry.getKey();
			    Integer value = entry.getValue();
			    
			}
		  
		  
		  System.out.println("before jar file send");
		  
		  
		  //////////////////// Send jar file to slaves ////////////////////////////
		  
		  String directory = "./jars";
		  File[] files = new File(directory).listFiles();
		  System.out.println(files.length);
		  for(int i=0;i<files.length;i++)
			  System.out.println(files);
		  
		  
		  for(File file1 : files)
		  {
			  System.out.println("inside for");
			  for (HashMap.Entry<String, Integer> entry : hash.entrySet())
			  {
			
				    String key = entry.getKey();
				    Integer value = entry.getValue();
				    
				    Socket socket = new Socket(key, value);
				    
				    DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
					FileInputStream fis = new FileInputStream(file1);
					
					dos.writeUTF("@#@jar");
					String name = file1.getName();
				    dos.writeUTF(name);
				    long length = file1.length();
				    dos.writeLong(length);
					
					byte[] buffer = new byte[4096];
					
					while (fis.read(buffer) > 0)
					{
						dos.write(buffer);
					
					}
					fis.close();
					dos.close(); 
				    
				}
			  
		      
		  }
		  
		
		  //Send method name to slave
		  Master m = new Master(20777);
		  System.out.println("Server Started");
		  m.start();
		  
	}
}





class ClientHandler extends Thread 
{
	
	Socket cs = null;
	Master m;

	public ClientHandler(Socket cs) throws IOException
	{
		m = new Master();
		this.cs = cs;
	}
	public void run() 
    {
		try {
			m.sendMethodCall(cs);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
