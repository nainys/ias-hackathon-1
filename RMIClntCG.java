
import java.io.FileWriter;
import java.io.IOException;
import java.lang.Class;
import java.lang.reflect.*;
import java.lang.*;

public class RMIClntCG {

	String className;
	int port;
	String hostname;

	public void methodImpl(Method method, FileWriter out, Class c) {
		Class returnType = method.getReturnType();
		Class[] parameterTypes = method.getParameterTypes();
		int mod = method.getModifiers() & ~Modifier.ABSTRACT;
		String modifier = Modifier.toString(mod);
		String hashed =  "";
		try {

			
			out.write(modifier + " " + returnType.getName() + " " + method.getName() + " (");

			int i = 1;
			int numOfParams = method.getParameterCount();
			for (Class pt:parameterTypes) {

				if(i == numOfParams) {
					out.write(pt.getName() +" param" + i);
				} else {
					out.write(pt.getName() +" param" + i + ", ");
				}

				i++;
        	}



			out.write(")\n{\n");
			out.write("try {\n");
			i=1;
			for (Class pt:parameterTypes) {
				switch (pt.getName()) {
				case "int":
					hashed+="int@#@";
					hashed+="\"+p"+i+"+\"@#@";
					out.write("\tInteger");
					out.write(" p" + i + " = new ");
					out.write("Integer(param" + i + ");");
					break;
				case "float":
					hashed+="float@#@";
					hashed+="\"+p"+i+"+\"@#@";
					out.write("\tFloat");
					out.write(" p" + i + " = new ");
					out.write("Float(param" + i + ");");
					break;
				case "double":
					hashed+="double@#@";
					hashed+="\"+p"+i+"+\"@#@";
					out.write("\tDouble");
					out.write(" p" + i + " = new ");
					out.write("Double(param" + i + ");");
					break;
				case "short":
					hashed+="short@#@";
					hashed+="\"+p"+i+"+\"@#@";
					out.write("\tShort");
					out.write(" p" + i + " = new ");
					out.write("Short(param" + i + ");");
					break;
				case "char":
					hashed+="char@#@";
					hashed+="\"+p"+i+"+\"@#@";
					out.write("\tCharacter");
					out.write(" p" + i + " = new ");
					out.write("Character(param" + i + ");");
					break;
				case "long":
					hashed+="long@#@";
					hashed+="+p"+i+"+@#@";
					out.write("\tLong");
					out.write(" p" + i + " = new ");
					out.write("Long(param" + i + ");");
					break;
				case "boolean":
					hashed+="boolean@#@";
					hashed+="\"+p"+i+"+\"@#@";
					out.write("\tBoolean");
					out.write(" p" + i + " = new ");
					out.write("Boolean(param" + i + ");");
					break;
				case "byte":
					hashed+="byte@#@";
					hashed+="\"+p"+i+"+\"@#@";
					out.write("\tByte");
					out.write(" p" + i + " = new ");
					out.write("Byte(param" + i + ");");
					break;

				default:
					hashed+="\"+p"+i+"+\"@#@";
					out.write("\t" + pt.getName());
					out.write(" p" + i + " = ");
					out.write("param" + i + ";");
					break;

				}
				out.write("\n");
				i++;

			}
			out.write("");
			out.write("\tSocket client = new Socket(\"" + this.hostname + "\", " + this.port + ");\n");
			
			i = 1;
			for (Class pt:parameterTypes) {
				i++;
			}
			hashed = hashed.substring(0,hashed.length()-3);
			out.write("\tString s = \"" + method.getName() +"@#@"+(i-1)+"@#@"+hashed+"\";\n");
			out.write("\tPrintStream ps = new PrintStream(client.getOutputStream());\n");
			out.write("\tps.println(s);\n");
			out.write("\tps.flush();\n");
			out.write("\tSystem.out.println(s);\n");
			out.write("\tPrintStream ps1 = new PrintStream(client.getOutputStream());\n");
			out.write("\tps1.println(\"Master return the result!\");\n");
			out.write("\tps1.flush();\n");
			out.write("\tBufferedReader br= new BufferedReader(new InputStreamReader( client.getInputStream()));\n");
			out.write("\tString str= br.readLine();\n");
			out.write("\tSystem.out.println(str);\n");
			out.write("\tps.close();\n");
			out.write("\tps1.close();\n");
			//out.write("\treturn str;\n");

			switch (returnType.getName()) {
			case "int":
				out.write("\tInteger res = Integer.parseInt(str);\n");
				out.write("\treturn res.intValue();\n");
				break;
			case "float":
				out.write("\tFloat res = Float.parseFloat(str);\n");
				out.write("\treturn res.floatValue();\n");
				break;
			case "double":
				out.write("\tDouble res = Double.parseDouble(str);\n");
				out.write("\treturn res.doubleValue();\n");
				break;
			case "short":
				out.write("\tShort res = Short.parseShort(str);\n");
				out.write("\treturn res.shortValue();\n");
				break;
			case "char":
				out.write("\tCharacter res = Char.parseChar(str);\n");
				out.write("\treturn res.charValue();\n");
				break;
			case "long":
				out.write("\tLong res = Long.parseLong(str);\n");
				out.write("\treturn res.longValue();\n");
				break;
			case "boolean":
				out.write("\tBoolean res = Boolean.parseBoolean(str);\n");
				out.write("\treturn res.booleanValue();\n");
				break;
			case "byte":
				out.write("\tByte res = Byte.parseByte(str);\n");
				out.write("\treturn res.byteValue();\n");
				break;

			default:
				out.write("\t" + returnType.getName() + " res = (" + returnType.getName() + ")ois.readObject();\n");
				out.write("\treturn res;\n");
				break;

			}


			out.write("} \ncatch (Exception e) {\n");
			out.write("\te.printStackTrace();\n");
			out.write("}\n");

			out.write("\treturn 0;\n");
			out.write("}\n");


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	public static void main(String[] args) {

		FileWriter out = null;

		RMIClntCG codeGen = new RMIClntCG();

		Class c;
		try {
			codeGen.className = args[0];
			codeGen.port = Integer.parseInt(args[1]);
			codeGen.hostname = args[2];
			c = Class.forName(codeGen.className);
			out = new FileWriter("ClntStub.java");


			out.write("import java.io.*;\n");
			out.write("import java.net.*;\n");
			out.write("import java.util.*;\n");
			out.write("\n");
			out.write("public class ClntStub implements " + c.getName() + "\n");
			out.write("{\n");

			Method[] methods = c.getMethods();
			 
	        // Printing method names
	        for (Method method:methods) {
	        	/*Class[] parameterTypes = method.getParameterTypes();
	        	for (Class pt:parameterTypes) {
	        		System.out.println(pt.getName());
	        	}
	            System.out.println(method.getName());*/
	        	out.write("\n");
	        	codeGen.methodImpl(method, out, c);
	        }
	        
	        
	        out.write("}\n");
	        out.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
